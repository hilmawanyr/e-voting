<?php

	function get_jur($key)
	{
		if ($key == '24201') {

			$prodi = 'TEKNIK KIMIA';
		} elseif ($key == '25201') {

			$prodi = 'TEKNIK LINGKUNGAN';
		} elseif ($key == '26201') {

			$prodi = 'TEKNIK INDUSTRI';
		} elseif ($key == '32201') {

			$prodi = 'TEKNIK PERMINYAKAN';
		} elseif ($key == '55201') {

			$prodi = 'TEKNIK INFORMATIKA';
		} elseif ($key == '61101') {

			$prodi = 'MAGISTER MANAJEMEN';
		} elseif ($key == '61201') {

			$prodi = 'MANAJEMEN';
		} elseif ($key == '62201') {

			$prodi = 'AKUNTANSI';
		} elseif ($key == '70201') {

			$prodi = 'ILMU KOMUNIKASI';
		} elseif ($key == '73201') {

			$prodi = 'PSIKOLOGI';
		} elseif ($key == '74101') {

			$prodi = 'MAGISTER HUKUM';
		} elseif ($key == '74201') {

			$prodi = 'ILMU HUKUM';
		} elseif ($key == '86206') {

			$prodi = 'PENDIDIKAN GURU SEKOLAH DASAR';
		} elseif ($key == '85202') {

			$prodi = 'PENDIDIKAN KEPELATIHAN OLAHRAGA';
		} else {
			$prodi = '-';
		}

		return $prodi;
	}

	function get_student_name($npm)
	{
		$CI =& get_instance();
		$CI->load->model('vote_model');
		$getName = $CI->vote_model->catch_student_name($npm);
		return $getName;
	}