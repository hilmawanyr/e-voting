<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vote_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function catch_student_name($npm)
	{
		$url            = APIHOST."api/api_voting/student_name/".$npm;
		$request_headers= ['x-voting-key: YXBwX2tleV9jaGF0Ym9vdA'];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
		$response = curl_exec($ch);
        curl_close($ch);
        $decodeData = json_decode($response);
        return $decodeData->result;
	}

}

/* End of file Vote_model.php */
/* Location: ./application/models/Vote_model.php */