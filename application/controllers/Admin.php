<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index()
	{
		// prevent error division by zero
		error_reporting(0);

		$data['activeStudentAmount'] = $this->_getActiveStudentAmount();
		$data['voteAmount']          = $this->db->get('vote')->num_rows();
		$data['votingPercentage']    = ($data['voteAmount'] / $data['activeStudentAmount']) * 100;
		$data['amountParmaChooser']  = $this->db->where('candidate', 1)->get('vote')->num_rows();
		$data['amountDemaChooser']   = $this->db->where('candidate', 2)->get('vote')->num_rows();

		$totalVotes = $data['amountParmaChooser'] + $data['amountDemaChooser'];
		$data['percentageOfParma'] = ($data['amountParmaChooser'] / $totalVotes) * 100;
		$data['percentageOfDema'] = ($data['amountDemaChooser'] / $totalVotes) * 100;
		
		$this->load->view('home_v', $data);
	}

	private function _getActiveStudentAmount()
	{
		$url            = APIHOST."api/api_voting/amount_of_active_student";
		$request_headers= ['x-voting-key: YXBwX2tleV9jaGF0Ym9vdA'];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
		$response = curl_exec($ch);
        curl_close($ch);
        $decodeData = json_decode($response);
        return $decodeData->result;
	}

	public function export_to_excel()
	{
		$data['report'] = $this->db->query("SELECT department from vote group by department")->result();
		$this->load->view('excel_report', $data);
	}

	public function detail_export_to_excel()
	{
		$data['report'] = $this->db->query("SELECT * from vote")->result();
		$this->load->view('export_detail', $data);
	}

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */