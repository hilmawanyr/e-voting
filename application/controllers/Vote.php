<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vote extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		$this->load->view('vote_v');
	}

	public function submit()
	{
		$id      = $this->input->post('id');
		$pass    = $this->input->post('pass');
		$parties = $this->input->post('partai');

		$authorization = $this->_cURL($id, $pass);
		$authData = json_decode($authorization);

		// if user doesn't exist or have inactive
		if ($authData->status != 1) {
			$this->session->set_flashdata('fail', 'Otentikasi gagal!');
			redirect(base_url());
		}

		// is user have voting ?
		$isUserHaveVoting = $this->db->where('responden_id', $id)->get('vote')->num_rows();
		if ($isUserHaveVoting > 0) {
			$this->session->set_flashdata('have_vote', 'Anda sudah melakukan voting  sebelumnya!');
			redirect(base_url());
		}

		$userData = [
			'candidate' => $parties,
			'responden_id' =>$authData->data->userid,
			'year' => date('Y'),
			'department' => $authData->data->prodi
		];
		$this->db->insert('vote', $userData);

		echo '<script>alert("Berhasil!");history.go(-1);</script>';
	}

	private function _cURL($npm, $pass)
	{
		$url = APIHOST."api/api_voting/attemp_login";
		$payload = ['npm' => $npm, 'password' => $pass];

		$request_headers = ['x-voting-key: YXBwX2tleV9jaGF0Ym9vdA'];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
		$response = curl_exec($ch);

        curl_close($ch);
        return $response;
	}

}

/* End of file Vote.php */
/* Location: ./application/controllers/Vote.php */