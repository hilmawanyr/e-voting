<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>e-Voting</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-black-light layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?= base_url() ?>" class="navbar-brand">
            <b>e</b>VOTING
          </a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>
      </div>
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">

      <!-- Main content -->
      <section class="content">
        <div class="callout callout-info">
          <h4>Selamat Datang!</h4>

          <p>Ini adalah aplikasi <i>electronic voting (e-voting)</i> yang diperuntukan untuk pemilihan ketua dan wakil ketua sebuah organisasi. Silahkan isi ID beserta dan <i>password</i> anda sebelum melakukan voting guna identifikasi responden. Terimakasih.</p>
        </div>

        <?php if ($this->session->userdata('fail')) { ?>
          <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Gagal!</h4> Otentikasi gagal. Akun tidak ditemukan atau sudah tidak aktif!
          </div>
        <?php } ?>

        <?php if ($this->session->userdata('have_vote')) { ?>
          <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Ditolak!</h4> <?= $this->session->userdata('have_vote'); ?>
          </div>
        <?php } ?>
        
        <div class="row">
          <div class="col-md-4">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Isi dengan akun SIA</h3>
              </div>
              <form action="<?= base_url('submit_vote') ?>" method="POST">
                <div class="box-body">
                  <div class="form-group">
                    <label for="npm">ID</label>
                    <input type="text" class="form-control" id="id" name="id" placeholder="Masukan ID" required="">
                  </div>
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="pass" placeholder="Masukan pasword" required="">
                  </div>
                  <div class="form-group">
                    <label for="password">Pilih Partai</label>
                    <select class="form-control" name="partai" required="">
                      <option disabled="" selected=""></option>
                      <option value="1">Partai PARMA</option>
                      <option value="2">Partai DEMA</option>
                    </select>
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
              <!-- /.box-body -->
            </div>
          </div>
          <div class="col-md-8">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Partai Pilihan</h3>
              </div>
              <div class="box-body">
                <div class="col-md-6">
                  <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-info">
                      <div class="widget-user-image">
                        <img class="img-circle" src="<?= base_url('assets/images/parma.jpg') ?>" alt="User Avatar">
                      </div>
                      <!-- /.widget-user-image -->
                      <h3 class="widget-user-username">PARMA</h3>
                      <h5 class="widget-user-desc">Partai Mahasiswa</h5>
                    </div>
                    <div class="box-footer no-padding">
                      <ul class="nav nav-stacked">
                        <li><a href="#parma" data-toggle="modal">Lihat Foto Kandidat <span class="pull-right badge bg-blue">1</span></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <dic class="col-md-6">
                  <div class="box box-widget widget-user-2">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-info">
                      <div class="widget-user-image">
                        <img class="img-circle" src="<?= base_url('assets/images/dema.jpg') ?>" alt="User Avatar">
                      </div>
                      <!-- /.widget-user-image -->
                      <h3 class="widget-user-username">DEMA</h3>
                      <h5 class="widget-user-desc">Demokrasi Mahasiswa</h5>
                    </div>
                    <div class="box-footer no-padding">
                      <ul class="nav nav-stacked">
                        <li><a href="#dema" data-toggle="modal">Lihat Foto Kandidat <span class="pull-right badge bg-blue">1</span></a></li>
                      </ul>
                    </div>
                  </div>
                </dic>
              </div>
              <!-- /.box-body -->
            </div>
          </div>
        </div>
        
        <!-- /.box -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> 0.0.1
      </div>
      <strong>Copyright &copy; 2019 <a href="https://adminlte.io">UNIVERSITAS BHAYANGKARA JAKARTA RAYA</a>.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<div class="modal fade" id="dema">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Kandidat Partai DEMA</h4>
      </div>
      <div class="modal-body">
        <img 
          src="<?= base_url('assets/images/kandidat1.jpg') ?>" 
          style="width: 570px; height: 570px; object-fit: cover; margin: auto;"
          alt="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="parma">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Kandidat Partai PARMA</h4>
      </div>
      <div class="modal-body">
        <img 
          src="<?= base_url('assets/images/kandidat2-2.jpg') ?>" 
          style="width: 50%; object-fit: scale-down; float: left;"
          alt="">
        <img 
          src="<?= base_url('assets/images/kandidat2-1.jpg') ?>" 
          style="width: 50%; object-fit: scale-down;"
          alt="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- jQuery 3 -->
<script src="<?= base_url('assets/') ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url('assets/') ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?= base_url('assets/') ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url('assets/') ?>bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets/') ?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('assets/') ?>dist/js/demo.js"></script>
</body>
</html>
