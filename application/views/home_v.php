<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>e-Voting</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url('assets/') ?>dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-black-light layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?= base_url() ?>" class="navbar-brand">
            <b>e-</b>VOTING
          </a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>
      </div>
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">

      <!-- Main content -->
      <section class="content">

        <div class="row">
          <div class="col-md-12">
            <div class="callout callout-info">
              <h4>Selamat Datang!</h4>
              <p>Ini merupakan halam admin dari aplikasi e-Voting. Untuk mendapatkan laporan terbaru Anda dapat me-<i>refresh</i> atau me-<i>reload</i> halaman. Untuk dapat mengekpor data laporan kedalam bentuk file excel, Anda dapat menggunakan tombol <b>Export Report To Excel</b> dibagian bawah halaman.</p>
            </div>
          </div>
          
        </div>
        
        <div class="row">
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-aqua"><i class="fa fa-group"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Total Mahasiswa Aktif</span>
                <span class="info-box-number"><?= $activeStudentAmount ?></span>
              </div>
            </div>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-aqua"><i class="fa fa-comment-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Total Suara</span>
                <span class="info-box-number"><?= $voteAmount ?></span>
              </div>
            </div>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-aqua"><i class="fa fa-pie-chart"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Persentase Pemilih</span>
                <span class="info-box-number"><?= number_format($votingPercentage, 2) ?> %</span>
              </div>
            </div>
          </div>
        </div>
        
        <div class="row">

          <div class="col-md-6">
            <div class="info-box">
              <span class="info-box-icon bg-aqua"><i class="fa fa-bar-chart"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Persentase Pemilih PARMA</span>
                <span class="info-box-number">
                  <?= number_format($percentageOfParma, 2) ?> % (<?= $amountParmaChooser ?> suara)
                </span>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="info-box">
              <span class="info-box-icon bg-aqua"><i class="fa fa-bar-chart"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Persentase Pemilih DEMA</span>
                <span class="info-box-number"><?= number_format($percentageOfDema, 2) ?> % (<?= $amountDemaChooser ?> suara)</span>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <a class="btn btn-block bg-olive" href="<?= base_url('export_excel') ?>">
              <i class="fa fa-file-excel-o"></i> Export Report To Excel
            </a>
          </div>
          <div class="col-md-6">
            <a class="btn btn-block bg-yellow" href="<?= base_url('detail_export_excel') ?>">
              <i class="fa fa-file-excel-o"></i> Export Detail Report To Excel
            </a>
          </div>

        </div>

      </section>
    </div>
  </div>

  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> 0.0.1
      </div>
      <strong>Copyright &copy; 2019 <a href="https://adminlte.io">UNIVERSITAS BHAYANGKARA JAKARTA RAYA</a>.</strong> All rights
      reserved.
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<div class="modal fade" id="dema">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Kandidat Partai DEMA</h4>
      </div>
      <div class="modal-body">
        <img 
          src="<?= base_url('assets/images/kandidat1.jpg') ?>" 
          style="width: 570px; height: 570px; object-fit: cover; margin: auto;"
          alt="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="parma">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Kandidat Partai PARMA</h4>
      </div>
      <div class="modal-body">
        <img 
          src="<?= base_url('assets/images/kandidat2-2.jpg') ?>" 
          style="width: 50%; object-fit: scale-down; float: left;"
          alt="">
        <img 
          src="<?= base_url('assets/images/kandidat2-1.jpg') ?>" 
          style="width: 50%; object-fit: scale-down;"
          alt="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- jQuery 3 -->
<script src="<?= base_url('assets/') ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url('assets/') ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?= base_url('assets/') ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url('assets/') ?>bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets/') ?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('assets/') ?>dist/js/demo.js"></script>
</body>
</html>
