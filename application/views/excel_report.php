<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=Export_Vote_Report.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<table border="2">
	<tr>
		<td rowspan="2">No</td>
		<td rowspan="2">Prodi</td>
		<td colspan="2">Jumlah Pemilih</td>
	</tr>
	<tr>
		<td>PARMA</td>
		<td>DEMA</td>
	</tr>
	<?php $no = 1; foreach ($report as $val) { ?>
		<tr>
			<td><?= $no ?></td>
			<td><?= get_jur($val->department) ?></td>
			<td>
				<?php $parma = $this->db->where('department', $val->department)->where('candidate',1)->get('vote')->num_rows(); echo $parma; ?>
			</td>
			<td>
				<?php $dema = $this->db->where('department', $val->department)->where('candidate',2)->get('vote')->num_rows(); echo $dema; ?>
			</td>
		</tr>
	<?php $no++; } ?>
</table>