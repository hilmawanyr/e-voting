<?php
header("Content-Type: application/xls");    
header("Content-Disposition: attachment; filename=Export_Vote_Report_Detail.xls");  
header("Pragma: no-cache"); 
header("Expires: 0");
?>

<table border="2">
	<tr>
		<td>No</td>
		<td>NPM</td>
		<td>Nama</td>
		<td>Prodi</td>
		<td>Kandidat Dipilih</td>
		<td>Waktu Transaksi Data</td>
	</tr>
	<?php $no = 1; foreach ($report as $val) { ?>
		<tr>
			<td><?= $no ?></td>
			<td><?= $val->responden_id ?></td>
			<td><?= get_student_name($val->responden_id) ?></td>
			<td><?= get_jur($val->department) ?></td>
			<td><?= $val->candidate == 1 ? 'PARMA' : 'DEMA' ?></td>
			<td><?= $val->created_at ?></td>
		</tr>
	<?php $no++; } ?>
</table>